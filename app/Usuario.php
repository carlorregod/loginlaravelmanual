<?php

namespace App;

use Illuminate\Notifications\Notifiable;
//Para hashear la pw
use Illuminate\Support\Facades\Hash;

use Illuminate\Database\Eloquent\Model;
//Incluir para el login
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

class Usuario extends Model implements AuthenticatableContract
{
	use Notifiable, Authenticatable;

    protected $table = 'usuarios';

    protected $fillable = [
        'id', 'nombre', 'email', 'user', 'password', 'rol'
    ];
    protected $primaryKey ='id';

    protected $hidden = [
        'password', 'remember_token',
    ];
    //Constructor de pw encriptados
    public function setPasswordAttribute($pass) 
    {
        $this->attributes['password'] = Hash::make($pass);
    }

}

