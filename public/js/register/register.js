limpiaformulario = function() {
	$('#nombre').val('');
	$('#email').val('');
	$('#user').val('');
	$('#pass1').val('');
	$('#pass2').val('');
	$('#nombre').focus();
	return;
}

fn_respuesta=function(data) {
	if(data.idmensaje == 1)
	{
		alert(data.mensaje);
		return;
	}
	else
	{
		alert(data.mensaje);
		limpiaformulario();
	}	
};


registro = function() {
	var nombre = $('#nombre').val();
	var email = $('#email').val();
	var user = $('#user').val();
	var password = $('#pass1').val();
	var pass2 = $('#pass2').val();
	
	if(nombre =='' || email ==''|| user ==''|| password ==''|| pass2 =='')
		{
			alert('Complete todos los campos del formulario');
			return;
		}
	//Validaciones con funciones externas
	
	if(!revisaNombreApellido(nombre))
		{
		alert('Ingrese un nombre válido')
		return;
		}
	if(!revisaEmail(email))
	{
		alert('Correo debe ser válido de la forma alguien@mail.com')
		return;
	}
	if(!revisaUser(user))
		return;
	//Validar pw coincidente
	if(password != pass2)
		{
			alert('Password no coinciden');
			return;
		}
	if(!revisaPassword(password) || !revisaPassword(pass2))
		return;
	//El AJAX
		parametros={
				'nombre':nombre,
				'email':email,
				'user':user,
				'password':password
		};
		url="/register/post";
		method="POST";
		ajaxCallback(parametros, url, method, fn_respuesta);
		return;
		
};

$('#btnEnviar').click(registro);