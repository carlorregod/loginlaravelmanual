<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Llamado al request
use App\Http\Requests\nuevoRegistro;
use App\Http\Requests\loginResource;
//Llamado al modelo
use App\Usuario;
//Para el login
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Session;
use Redirect;


class loginController extends Controller
{
	//Muchas vistas!
    public function index()
    {
    	return view('index');
    }

    public function register()
    {
    	return view('register');
    }

    //Métodos de inserción de nuevo user
    public function new_reg(nuevoRegistro $request)
    {

    	if($request->ajax())
    	{
    		//Objeto a contar del modelo Usuario
	    	$usuario = new Usuario();
	    	//Verificar que el usuario no esté en uso
	    	//Ve si hay usuarios con una query
	    	$query = Usuario::where('user','=',$request->user)->first();
	    	//Si hay retorna... arrojando mensaje de no inserción
	    	if($query)
	    		return response()->json(['idmensaje'=>'1','mensaje'=>'El usuario está en uso actualmente']);
	    	//Si se salta el if... se sigue la inserción
	    	$usuario->rol 		= 1; //implica un rol de usuario normal
	    	$usuario->user 		= $request->user;
	    	$usuario->nombre 	= $request->nombre;
	    	$usuario->email 	= $request->email;
	    	$usuario->password 	= $request->password;

	    	$usuario->save(); //Guardamos el registro
	    	return response()->json(['idmensaje'=>'0','mensaje'=>'Los datos han sido añadidos']); //Regreso al AJAX
    	}
    }

    public function login(loginResource $request)
    {
    	if(Auth::attempt(['user'=>$request->user, 'password'=>$request->password]))
    		return Redirect::to('/main');
    	else
    	{
    		Session::flash('message-error','Usuario y/o contraseña errados. Vuelva a ingresar.');
    		return Redirect::to('/');
    	}
    }

}
