<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Bienvenido</title>

        <!-- Fonts -->

        <link href="css/default.css" rel="stylesheet" type="text/css">

    </head>
    <body>
        <h2 class="p-3 mb-2 bg-primary text-white">Bienvenido entraste {!!auth()->user()->user!!}</h2>
        <form method="POST" action="/login/logout">
        {{ csrf_field() }}
       		<button name="btnSalir" id="btnSalir" class="btn btn-primary">Salir</button>
		</form>

    </body>
</html>
