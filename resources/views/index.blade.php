<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Bienvenido</title>
    </head>
    <body>
      @include('alertas-coleccion.error')
        <h2 class="p-3 mb-2 bg-primary text-white">Ingresar</h2>

       <table class="my-3 mx-5 center">    
       <form action="/login/post" method="POST"> 
       {{ csrf_field() }}
       	<tr>
       		<td><label for="nombre">Usuario</label></td>
       		<td><input type="text" name="user" id="user" maxlength="100" required>
       	</tr>
       	<tr>
       		<td><label for="password">Contraseña</label></td>
       		<td><input type="password" name="password" id="password" required>
       	</tr>
       	<tr>
       		<td class="py-3" colspan="2">
          <button name="btnEnviar" id="btnEnviar" class="btn btn-primary btn-block">Ingresar</button>
          </td>
       	</tr>
      </form>
        </td>
       	<tr>  		
       		<td class="py-3" colspan="2">
       			<form method="GET" action="/register">
       			<button name="btnRegistrar" id="btnRegistrar" class="btn btn-primary btn-block">Registrarse</button>
       			</form>
   			  </td>     			
       	</tr>      
       </table>
    </body>
</html>
