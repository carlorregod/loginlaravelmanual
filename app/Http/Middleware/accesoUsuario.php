<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class accesoUsuario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //rol==1 implica un usuario normal
        if (auth()->check() && auth()->user()->rol==1) {
            return $next($request);
        }
        else{
            return redirect('/');
        }
        
        
    }
}
