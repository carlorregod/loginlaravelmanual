<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Para el logout
use Auth;
use Redirect;


class mainController extends Controller
{
	//Ruta protegida con un constructor de middleware
	public function __construct()
	{
	    $this->middleware('accesoUsuario');
	}

    public function index()
    {
    	return view('main');
    }

    public function logout()
    {
    	//Salida del autentificador y retorno al login
    	Auth::logout();
    	return Redirect::to('/');

    }
}
