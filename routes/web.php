<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','loginController@index')->name('login');
Route::get('/register','loginController@register');
//Crear registro
Route::post('/register/post','loginController@new_reg');
//Protocolo de acceso al sistema
Route::post('/login/post','loginController@login');
//Protocolo de salida del sistema
Route::post('login/logout','mainController@logout');

Route::get('/main','mainController@index'); //Menú principal